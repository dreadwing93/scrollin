////////////////////////////////////////////////////////////////
// CANVAS
////////////////////////////////////////////////////////////////
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");


var prev_time = Date.now();

//start the main loop
setInterval(main, 1000 / 30);
 //the number here is the amount of milliseconds to wait before each loop. '1000 / 30' will set it to approximately 30 fps.

// The main game loop
function main () {
	var current_time = Date.now();
	var delta = current_time - prev_time;
 //the delta variable is the ammount of milliseconds passed since the last loop.
 //it is used to make our game independant of framerate.
	update(delta / 1000);// dividing delta by 1000 converts it to seconds.
	draw();

	prev_time = current_time;
}

function update(delta){
 //stub
}

function draw(){
 context.clearRect(0, 0, canvas.width, canvas.height);//clear the screen
 context.fillStyle="#FF0000";
 context.fillRect(0, 0, canvas.width, canvas.height); 
 
}
